package com.kulak.fishes

import java.awt.BorderLayout
import javax.swing.JFrame
import javax.swing.WindowConstants

class MainFrame : JFrame() {
    init {
        setSize(900, 600)
        title = "Catch fish if you can:D"
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        val fishTab = FishTab()
        add(fishTab, BorderLayout.CENTER)
    }
}

fun main() {
    val app = MainFrame()
    app.isVisible = true
}
