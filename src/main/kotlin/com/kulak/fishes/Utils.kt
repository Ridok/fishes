package com.kulak.fishes

import java.awt.Image
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

fun getResizedImg(img: Image, w: Int, h: Int): BufferedImage {
    val resizedImg = BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
    val g2d = resizedImg.createGraphics()
    g2d.drawImage(img, 0, 0, w, h, null)
    g2d.dispose()
    return resizedImg
}

fun loadImage(path: String, clazz: Class<out Any>): BufferedImage {
    return ImageIO.read(clazz.classLoader.getResource(path))
}

fun deepCopy(bi: BufferedImage): BufferedImage {
    val cm = bi.colorModel
    val isAlphaPremultiplied = cm.isAlphaPremultiplied
    val raster = bi.copyData(null)
    return BufferedImage(cm, raster, isAlphaPremultiplied, null)
}
