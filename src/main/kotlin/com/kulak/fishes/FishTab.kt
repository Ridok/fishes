package com.kulak.fishes

import com.kulak.fishes.entities.DeadFish
import com.kulak.fishes.entities.HunterFish
import com.kulak.fishes.entities.RedFish
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Toolkit
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.image.BufferedImage
import javax.swing.JLabel
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.JTextField
import javax.swing.Timer
import kotlin.math.min
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class FishTab : JPanel() {
    private val log: Logger = LoggerFactory.getLogger(FishTab::class.java)
    private lateinit var redFishes: MutableList<RedFish>
    private lateinit var deadFishes: MutableList<DeadFish>
    private val screenSize = Toolkit.getDefaultToolkit().screenSize
    private var hunterFish = HunterFish(0, 0, 0, 100, loadImage("big.png",
        HunterFish::class.java), 0.0, false)
    private var isStarted = false
    private var isFinished = false
    private val newGamePanel = JPanel()
    private val countInput = JTextField(13)
    private var count: Int = 0
    private var buffer: BufferedImage = BufferedImage(screenSize.width, screenSize.height, BufferedImage.TYPE_INT_ARGB)

    init {
        this.layout = BorderLayout()
        this.background = Color.WHITE
        val ask = JLabel("How much fish do you want to catch")
        add(newGamePanel, BorderLayout.NORTH)
        newGamePanel.add(ask, BorderLayout.NORTH)
        countInput.layout = BorderLayout()
        newGamePanel.add(countInput, BorderLayout.SOUTH)
        countInput.addActionListener(NewGameActionListener())
        this.addMouseMotionListener(HunterFishMouseAdapter())
    }

    override fun paint(g: Graphics) {
        super.paint(g)
        g.drawImage(buffer, 0, 0, null)
        when {
            isStarted && !isFinished -> {
                (g as Graphics2D).drawImage(loadImage("see.png", FishTab::class.java), 0, 0, null)
                redFishes.forEach { it.draw(g) }
                deadFishes.forEach { it.draw(g) }
                hunterFish.draw(g)
                g.font = Font("TimesRoman", Font.PLAIN, 10)
                g.color = Color.BLACK
                g.drawString("Score:\nCaught:${deadFishes.size}\nLeft:${count - deadFishes.size}",
                    (this.size.width * 0.9).toInt(), (this.size.height * 0.9).toInt())
                newGamePanel.isVisible = true
            }
            !isStarted && isFinished -> {
                g.font = Font("TimesRoman", Font.PLAIN, 50)
                g.color = Color.BLACK
                val panelCenter = min(this.size.height, this.size.width) / 2
                g.drawString("The END", panelCenter, panelCenter)
                newGamePanel.isVisible = true
            }
        }
    }

    private inner class HunterFishMouseAdapter : MouseAdapter() {
        override fun mouseDragged(e: MouseEvent) {
            hunterFish.currentX = e.x
            hunterFish.currentY = e.y
            hunterFish.live = true
            redFishes
                .filter { it.isKilled(hunterFish.currentX, hunterFish.currentY) }
                .onEach { deadFishes.add(it.dead()) }
                .toList()
                .forEach { redFishes.remove(it) }
            when (deadFishes.size) {
                count -> {
                    isStarted = false; isFinished = true
                    log.info("Game was finished.")
                    count = 0
                }
            }
            repaint()
        }
    }

    private inner class NewGameActionListener : ActionListener {
        override fun actionPerformed(e: ActionEvent?) {
            try {
                log.info("New game was started.")
                isStarted = true
                isFinished = false
                count = countInput.text.toInt()
                countInput.text = ""
                redFishes = ArrayList()
                deadFishes = ArrayList()
                repeat(count) { redFishes.add(RedFish(this@FishTab)) }
                log.info("$count of red fishes were created.")
                Timer(100) { redFishes.forEach { it.move(); repaint() } }.start()
            } catch (e: NumberFormatException) {
                JOptionPane.showMessageDialog(null, "${e.message} try again later.")
                log.warn("${e.message} try again later.")
            }
            newGamePanel.isVisible = false
        }
    }
}
