package com.kulak.fishes.entities

import com.kulak.fishes.getResizedImg
import java.awt.Graphics
import java.awt.image.BufferedImage

abstract class Fish(
    var speed: Int,
    var currentX: Int,
    var currentY: Int,
    var size: Int,
    var img: BufferedImage,
    var angle: Double
) {
    init {
        img = getResizedImg(img, img.width * size / 100, img.height * size / 100)
    }

    abstract fun draw(g: Graphics)
}
