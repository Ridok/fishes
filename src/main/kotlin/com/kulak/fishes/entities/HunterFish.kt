package com.kulak.fishes.entities

import java.awt.Graphics
import java.awt.image.BufferedImage

class HunterFish(
    speed: Int,
    currentX: Int,
    currentY: Int,
    size: Int,
    img: BufferedImage,
    angle: Double,
    var live: Boolean
) :
    Fish(speed, currentX, currentY, size, img, angle) {
    override fun draw(g: Graphics) {
        if (live) {
            g.drawImage(this.img, this.currentX - 300, this.currentY - 100, null)
            live = false
        }
    }
}
