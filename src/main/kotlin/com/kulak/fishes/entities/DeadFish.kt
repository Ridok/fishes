package com.kulak.fishes.entities

import com.kulak.fishes.getResizedImg
import com.kulak.fishes.loadImage
import java.awt.Graphics
import java.awt.image.BufferedImage

class DeadFish(speed: Int, currentX: Int, currentY: Int, size: Int, img: BufferedImage, angle: Double) :
    Fish(speed, currentX, currentY, size, img, angle) {
    init {
        this.speed = 0
        this.img = getResizedImg(loadImage("dead.png", RedFish::class.java),
            img.width * size / 100, img.height * size / 100)
    }

    override fun draw(g: Graphics) {
        g.drawImage(img, currentX, currentY, null)
    }
}
