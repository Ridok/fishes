package com.kulak.fishes.entities

import com.kulak.fishes.deepCopy
import com.kulak.fishes.loadImage
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.util.Random
import javax.swing.JPanel
import kotlin.math.cos
import kotlin.math.sin

class RedFish(
    private val panel: JPanel,
    speed: Int = Random().nextInt(20) + 10,
    currentX: Int = Random().nextInt(panel.size.width),
    currentY: Int = Random().nextInt(panel.size.height),
    size: Int = 99,
    img: BufferedImage = loadImage("small.png", RedFish::class.java),
    angle: Double = Random().nextInt(12) * 30.0
) :
    Fish(speed, currentX, currentY, size, img, angle) {
    override fun draw(g: Graphics) {
        val backup = (g as Graphics2D).transform
        var imgToRotate = deepCopy(img)
        if (angle in 270.0..360.0 || angle in 0.0..90.0) {
            val tx = AffineTransform.getScaleInstance(1.0, -1.0)
            tx.translate(0.0, -imgToRotate.height.toDouble())
            val op = AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR)
            imgToRotate = op.filter(imgToRotate, null)
        }
        val a = AffineTransform.getRotateInstance(Math.toRadians(angle) + Math.PI,
            (imgToRotate.width / 2 + currentX).toDouble(),
            (imgToRotate.height / 2 + currentY).toDouble())
        g.transform = a
        g.drawImage(imgToRotate, currentX, currentY, null)
        g.transform = backup
    }

    fun dead(): DeadFish = DeadFish(speed, currentX, currentY, size, img, angle)

    fun isKilled(x: Int, y: Int): Boolean = x > currentX && x < currentX + img.width &&
        y > currentY && y < currentY + img.height

    fun move() {
        val deltaX = (speed * cos(Math.toRadians(angle))).toInt()
        val deltaY = (speed * sin(Math.toRadians(angle))).toInt()
        currentX = changeDirection(currentX, panel.size.width, deltaX)
        currentY = changeDirection(currentY, panel.size.height, deltaY)
    }

    override fun equals(other: Any?): Boolean = when (other) {
        is Fish -> other.currentX == currentX && other.currentY == currentY &&
            other.speed == speed && other.angle == angle
        else -> false
    }

    override fun hashCode(): Int {
        var result = speed
        result = 31 * result + currentX
        result = 31 * result + currentY
        result = 31 * result + size
        result = 31 * result + img.hashCode()
        result = 31 * result + angle.hashCode()
        return result
    }

    private fun changeDirection(i: Int, max: Int, delta: Int): Int = when {
        i < 0 && delta < 0 -> {
            angle = Random().nextInt(12) * 30.0; max
        }
        i > max && delta > 0 -> {
            angle = Random().nextInt(12) * 30.0; 0
        }
        else -> i + delta
    }
}
