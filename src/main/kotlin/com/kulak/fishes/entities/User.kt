package com.kulak.fishes.entities

data class User(val userName: String, val password: String, val score: Int = 0)
